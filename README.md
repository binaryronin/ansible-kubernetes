### Ansible Role for Kubernetes

Very simple role I wrote for getting the necessary binaries and configuration in place to spin up a Kubernetes cluster. I know there are more automated ways to do this, but this works as a simple way to get to the point of running kubeadm commands.

Requires that the EPEL repos are available for Docker installation
